//[SECTION] Packages and Dependencies
  const express = require("express"); 
  const mongoose = require("mongoose");  
  const dotenv = require("dotenv"); 
  const cors = require("cors");
  const productRoutes = require('./routes/product'); 
  const userRoutes = require('./routes/user');
  const orderRoutes = require('./routes/order');

//[SECTION] Server Setup
  const app = express(); 
  dotenv.config(); 
  app.use(cors())
  app.use(express.json());
  const secret = process.env.CONNECTION_STRING;
  const port = process.env.PORT; 


//[SECTION] Application Routes
  app.use('/users',userRoutes); 
  app.use('/products',productRoutes);
  app.use('/orders',orderRoutes);


//[SECTION] Database Connection
  mongoose.connect(secret);
  let connectStatus = mongoose.connection; 
  connectStatus.on('open', () => console.log('Database is Connected'));


//[SECTION] Gateway Response
  
  app.get('/', (req, res) => {
     res.send(`Welcome to Kabilang Kanto Store`); 
  }); 

  app.listen(port, () => console.log(`Server is running on port ${port}`)); 
