// [SECTION] Dependencies and Modules
const exp = require('express');
const auth = require('../auth');
const controller = require('../controllers/product');
const {verify, verifyAdmin} = auth;
const route = exp.Router();

route.post('/createProduct', verify, verifyAdmin, controller.createProduct);
route.get('/all', verify, controller.getAllProducts);
route.get('/:id', verify,  controller.getProduct);
route.put('/:id', verify, verifyAdmin, controller.updateProducts);
route.put('/:id/archive', verify, verifyAdmin, controller.deactivateProduct);
route.put('/:id/activate', verify, verifyAdmin, controller.activateProduct);



module.exports = route;
