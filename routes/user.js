//[SECTION] Dependencies and Modules
  const exp = require("express"); 
  const controller = require('./../controllers/user.js'); 
  const auth = require("../auth"); 
  const {verify, verifyAdmin} = auth;
//[SECTION] Routing Component
  const route = exp.Router(); 

  route.post('/register', controller.userReg)

  route.post('/login', controller.userLogin)
   

  route.get('/authorization',verify, verifyAdmin, controller.adminAuthorization)

  route.get('/info',verify,controller.getUserDetails)

  route.delete('/delete', controller.deletUserDatabase)


  //[SECTION] Expose Route System
  module.exports = route; 
