//[SECTION] Dependencies and Modules
   const mongoose = require('mongoose'); 

//[SECTION] Blueprint Schema
    const userSchema = new mongoose.Schema({
      fullname: {
        type: String,
        required: [true, 'Name is Required']
      },
      username: {
        type: String,
        required: [true, 'Name is Required']
      },
      email: {
        type: String,
        required: [true, 'Email is Required']
      },
      password: {
        type: String,
        required: [true, 'Password in Required']
      },
      mobileNo: {
        type: String,
        required: [true, 'Mobile Number is Required']
      },
      isAdmin: {
        type: Boolean,  
        default: false 
      },
      
    });


//[SECTION] Model 
  const User = mongoose.model('User', userSchema);
  module.exports = User;
