//[SECTION] Dependencies and Modules
	const User = require('../models/user');
	const auth = require('../auth');
	const bcrypt = require('bcrypt');
	const dotenv = require('dotenv');


//[SECTION] Environment Setup
  	dotenv.config();
  	const salt = Number(process.env.SALT); 

//[SECTION] Functionalities [CREATE]
//encrpyt validate insertMany
	module.exports.userReg = async (req,res) => {
		let newUser = new User({
				fullname:req.body.fullname,
				mobileNo:req.body.mobileNo,
      	username: req.body.username,
      	email: req.body.email,
      	password: bcrypt.hashSync(req.body.password, salt)
      	});
		let us = await User.findOne({username:req.body.username}).then(found => {
			if(found )
			{
				console.log(found.username);
				return res.send('Username already exist');
			}
			else {
				return true;
			}
		});
			
		let em = await User.findOne({email:req.body.email}).then(found => {
			if(found)		
				{
					console.log(found.email);
					return res.send ({message:"Email already exist"});
				}
				else
				{
				 	return true
				}
		  });
		let p =req.body.mobileNo+'';


		if(us && em ) {
			newUser.save().then((user, rejected) => {
		    		if (user)
		    		{
		    			return res.send(user); 
				    } else {
				        return res.send({message:"Failed to Register a new account"}); 
		      		}; 
		    	});
		}
		
	};

		
//[SECTION] Functionalities [RETRIEVE]
	 module.exports.userLogin = (req,res) => {
    	User.findOne({username:req.body.username}).then(user => {
    		if (user)
    		{
				const isPasswordCorrect = bcrypt.compareSync(req.body.password,user.password);
    			if(isPasswordCorrect)
    			{
    				return res.send({accessToken: auth.createAccessToken(user)});
    			}
    			else
    			{
    				return res.send({message:"Password is incorrect"});
    			}
    		}
    		else
    		{
    			return res.send({message:"Username does not exist"});
    		}
    	})

    	
  	};

  module.exports.adminAuthorization = (req,res) => {
  	let filter =  {username:req.body.username};
  	let update = {isAdmin:true};
  		return User.findOneAndUpdate(filter,update).then(user =>  {
    		return res.send({message:"User is now an admin"});	
    		

    	});
  };
			

module.exports.getUserDetails = (req, res) => {

    User.findOne({username:req.body.username}).then(result => res.send(result)).catch(err => res.send(err))
};

module.exports.deletUserDatabase = (req,res) => {
	User.deleteMany({}).then(result =>{
		return res.send({message:"User Successfully Deleted"});
	});
}